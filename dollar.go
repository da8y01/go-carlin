package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

const DOLAR_HOY = "http://dolarhoy.com/"
const DOLAR_SUF = "indexx.php"

var re_DOLAR_SELL = regexp.MustCompile(`([0-9\.]+)<\/font><\/b><\/font>`)

func SendDollar(from int) {
	buy, sell, err := dollar()
	if err != nil {
		return
	}
	SendMessage(fmt.Sprintf("buy: ARS %s - sell: ARS %s", buy, sell), from, "")
}

func dollar() (string, string, error) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", DOLAR_HOY, DOLAR_SUF), nil)
	if err != nil {
		return "", "", err
	}

	req.Header.Set("Referer", DOLAR_HOY)
	res, err := client.Do(req)
	if err != nil {
		return "", "", err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", "", err
	}
	sbody := string(body)
	sellLine := re_DOLAR_SELL.FindAllString(sbody, 2)
	if len(sellLine) < 2 {
		return "", "", errors.New("Dollar::empty values")
	}
	buy := re_DOLAR_SELL.ReplaceAllString(sellLine[0], "$1")
	sell := re_DOLAR_SELL.ReplaceAllString(sellLine[1], "$1")
	return buy, sell, nil
}
