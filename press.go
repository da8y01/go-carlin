package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

const URL_PRENSA_BASE = "http://po.org.ar"
const URL_PRENSA = URL_PRENSA_BASE + "/prensaObrera/ultima"
const URL_PRENSA_RSS = URL_PRENSA_BASE + "/prensaObrera/rss"

// telegram instant view url
const URL_PRENSA_IV = "https://t.me/iv?url=%s&rhash=585f0aa3e51abd"

var re_PRENSA_IMG = regexp.MustCompile(`.*<img src="([\/A-Za-z0-9_\.-]+)" class="img-responsive">.*`)
var re_PRENSA_LINKS = regexp.MustCompile(`.*<h2><a href="([\/A-Za-z0-9_\.-]+)">.*`)
var re_PRENSA_RSS = regexp.MustCompile(`.*<link>(http\:\/\/(www\.)?po\.org\.ar\/(prensaObrera|videos|comunicados)\/.*)<\/link>.*`)

func SendPress(from int, text string) {
	links, err := prensa(text == "news")
	if err != nil {
		return
	}
	for i := 0; i < len(links); i++ {
		SendMessage(links[i], from, "")
	}
}

func prensa(news bool) ([]string, error) {
	// @TODO ver videos y comunicados
	urll := URL_PRENSA
	re := re_PRENSA_LINKS
	if news {
		urll = URL_PRENSA_RSS
		re = re_PRENSA_RSS
	}

	resp, err := http.Get(urll)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	img := "http://po.org.ar/images/logo-prensa-obrera.png"
	if !news {
		imgLine := re_PRENSA_IMG.FindString(string(body))
		img = re_PRENSA_IMG.ReplaceAllString(imgLine, "$1")
	}

	links := re.FindAllString(string(body), -1)
	messages := make([]string, len(links)+1)
	messages[0] = img
	if !news {
		messages[0] = fmt.Sprintf("%s%s", URL_PRENSA_BASE, img)
	}
	for i := 0; i < len(links); i++ {
		link := re.ReplaceAllString(links[i], "$1")
		if !news {
			link = fmt.Sprintf("%s%s", URL_PRENSA_BASE, link)
		}
		link = fmt.Sprintf(URL_PRENSA_IV, url.PathEscape(link))
		messages[i+1] = link
	}
	return messages, nil
}
