package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"sort"
	"strings"
)

func SendSubway(from int) {
	status, err := subte()
	if err != nil {
		return
	}
	msg := make([]string, len(status))
	i := 0
	for k, v := range status {
		msg[i] = fmt.Sprintf("%c ➡️  %s", k, v)
		i++
	}
	sort.Strings(msg)
	out := fmt.Sprintf("<pre>%s</pre>", strings.Join(msg, "\n"))
	SendMessage(out, from, "HTML")
}

func subte() (map[rune]string, error) {
	// @TODO 'updated X time ago'
	out := map[rune]string{}
	resp, err := http.Get("http://enelsubte.com/estado/")
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	// BEGIN html table parse
	tableId := "<table id=\"tabla-estado\">"
	idx := strings.Index(string(body), tableId)
	body = body[idx+len(tableId):]
	idx = strings.Index(string(body), "</table>")
	body = body[:idx]
	// END html table parse
	reDiv := regexp.MustCompile("<div .*<\\/div>")
	status := reDiv.FindAllString(string(body), -1)
	reInner := regexp.MustCompile(">.*<")
	// spl := strings.Split(string(body), "</tr>")
	for i := 0; i < len(status); i += 2 {
		rstr0 := []rune(reInner.FindString(status[i]))
		rstr1 := []rune(reInner.FindString(status[i+1]))
		k := rstr0[1]
		val := string(rstr1[1 : len(rstr1)-1])
		out[k] = val
	}
	return out, nil
}
